package me.dablakbandit.mazegenerator;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class MazeConfiguration {
	private JavaPlugin plugin;
	private FileConfiguration conf = null;
	private File file = null;
	private String fname = null;
	
	public MazeConfiguration(JavaPlugin plugin, String filename){
		this.plugin = plugin;
		fname = filename;
	}
	
	public void ReloadConfig(){
		if(file==null){
			file = new File(plugin.getDataFolder(), fname);
		}
		conf = YamlConfiguration.loadConfiguration(file);
		
		InputStream isDefaults = plugin.getResource(fname);
		if(isDefaults!=null){
			YamlConfiguration confDefault = YamlConfiguration.loadConfiguration(isDefaults);
			conf.setDefaults(confDefault);
		}
	}
	
	public FileConfiguration GetConfig(){
		if(conf==null){
			ReloadConfig();
		}
		return conf;
	}
	
	public boolean SaveConfig(){
		if(conf==null||file==null){
			return false;
		}
		
		try{
			conf.save(file);
			return true;
		}catch(IOException ex){
			plugin.getLogger().log(Level.SEVERE, "[MazeGen] Error saving configuration file: '" + fname + "'!");
			return false;
		}
	}
	
	public File getFile(){
		return this.file;
	}
}
