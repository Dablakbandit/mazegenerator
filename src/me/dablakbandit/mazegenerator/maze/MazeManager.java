package me.dablakbandit.mazegenerator.maze;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.dablakbandit.mazegenerator.MazeGenerator;
import me.dablakbandit.mazegenerator.generator.GeneratorManager;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class MazeManager {

	public List<Maze> mazes = new ArrayList<Maze>();
	public List<Maze> removing = new ArrayList<Maze>();
	public Map<String, String> players = new HashMap<String, String>();
	public static MazeManager main = new MazeManager();

	private MazeManager(){
		File folder = new File(MazeGenerator.getInstance().getDataFolder() + "/mazes/");
		if(folder.exists()){
			for(File f : folder.listFiles()){
				if(f.getName().endsWith(".yml")){
					loadMaze(f.getName().substring(0, f.getName().length()-4));
				}
			}
		}
	}

	public static MazeManager getInstance(){
		return main;
	}
	
	public void save(){
		for(Maze m : mazes){
			m.save();
		}
	}

	public boolean loadMaze(String name){
		Maze maze = getMaze(name);
		if(maze!=null){
			return false;
		}
		if(!new File(MazeGenerator.getInstance().getDataFolder() + "/mazes/" + name + ".yml").exists()){
			return false;
		}
		maze = new Maze(name);
		maze.load();
		this.mazes.add(maze);
		return true;
	}

	public boolean unloadMaze(String name){
		Maze maze = getMaze(name);
		if(maze!=null){
			this.mazes.remove(maze);
			try{
				GeneratorManager.getInstance().removeGenerator(GeneratorManager.getInstance().getGenerator(maze));
			}catch(Exception e){
				
			}
			return true;
		}
		return false;
	}
	
	public boolean deleteMaze(String name){
		Maze maze = getMaze(name);
		if(maze!=null){
			this.mazes.remove(maze);
			maze.delete();
			return true;
		}
		return false;
	}
	
	public boolean deleteMaze(String name, Player player){
		Maze maze = getMaze(name);
		if(maze!=null){
			removing.add(maze);
			mazes.remove(maze);
			maze.delete(player);
			for(String pl : players.keySet()){
				if(players.get(pl).equalsIgnoreCase(name)){
					players.remove(pl);
				}
			}
			return true;
		}
		return false;
	}
	
	public void removeMaze(String name, Player s) {
		Maze maze = getMaze(name);
		if(maze!=null){
			maze.remove(s);
		}
	}

	public Maze createNewMaze(String name, Location location){
		Maze maze = new Maze(name, location);
		maze.setDate();
		this.mazes.add(maze);
		return maze;
	}
	
	public Map<String, List<String>> listMazes(){
		Map<String, List<String>> temp = new HashMap<String, List<String>>();
		List<String> loaded = new ArrayList<String>();
		List<String> unloaded = new ArrayList<String>();
		File folder = new File(MazeGenerator.getInstance().getDataFolder() + "/mazes/");
		if(folder.exists()){
			for(File f : folder.listFiles()){
				if(f.getName().endsWith(".yml")){
					String name = f.getName().substring(0, f.getName().length()-4);
					if(getMaze(name)!=null){
						loaded.add(name);
					}else{
						unloaded.add(name);
					}
				}
			}
		}
		temp.put("Loaded", loaded);
		temp.put("Unloaded", unloaded);
		return temp;
	}

	public Maze getMaze(String name){
		for(Maze m : mazes){
			if(m.getName().toLowerCase().equals(name.toLowerCase())){
				return m;
			}
		}
		return null;
	}

	public void setPlayerSelectedMaze(Player player, Maze maze){
		this.players.put(player.getUniqueId().toString(), maze.getName());
		player.sendMessage("[MazeGen] Maze " + maze.getName() + " selected");
	}

	public Maze getPlayerSelectedMaze(Player player){
		String uuid = player.getUniqueId().toString();
		if(this.players.containsKey(uuid)){
			Maze m = getMaze(this.players.get(uuid));
			if(m!=null){
				return m;
			}
			return null;
		}
		return null;
	}

	public boolean isLocationInAMaze(Location location){
		for(Maze maze : mazes){
			if(maze.isLocationIn(location)){
				return true;
			}
		}
		for(Maze maze : removing){
			if(maze.isLocationIn(location)){
				return true;
			}
		}
		return false;
	}

	public String isMazeOverlapping(Maze maze){
		for(Maze maze1 : mazes){
			if(maze!=maze1&&maze1.doesMazeOverlap(maze)){
				return "Maze overlaps " + maze1.getName();
			}
		}
		return null;
	}

}
