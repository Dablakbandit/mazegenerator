package me.dablakbandit.mazegenerator.maze;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.dablakbandit.mazegenerator.MazeConfiguration;
import me.dablakbandit.mazegenerator.MazeGenerator;
import me.dablakbandit.mazegenerator.generator.Generator;
import me.dablakbandit.mazegenerator.generator.GeneratorManager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class Maze {

	private MazeConfiguration config;
	private String name, date;
	private List<String> outerblocks = new ArrayList<String>(), innerblocks = new ArrayList<String>(), floorblocks = new ArrayList<String>(), roofblocks = new ArrayList<String>();
	private int width = 50, height = 10, path = 1;
	private boolean finished = false;
	private Location location;

	public Maze(String name){
		this.name = name;
		config = new MazeConfiguration(MazeGenerator.getInstance(), "mazes/" + name + ".yml");
		load();
	}

	public Maze(String name, Location location){
		this(name);
		this.location = location;
		save();
		check();
		setDate();
	}

	public void load(){
		if(config.GetConfig().isSet("InnerBlocks")){
			this.innerblocks = config.GetConfig().getStringList("InnerBlocks");
		}
		if(config.GetConfig().isSet("OuterBlocks")){
			this.outerblocks = config.GetConfig().getStringList("OuterBlocks");
		}
		if(config.GetConfig().isSet("FloorBlocks")){
			this.floorblocks = config.GetConfig().getStringList("FloorBlocks");
		}
		if(config.GetConfig().isSet("RoofBlocks")){
			this.roofblocks = config.GetConfig().getStringList("RoofBlocks");
		}
		if(config.GetConfig().isSet("Width")){
			this.width = config.GetConfig().getInt("Width");
		}
		if(config.GetConfig().isSet("Height")){
			this.height = config.GetConfig().getInt("Height");
		}
		if(config.GetConfig().isSet("Path")){
			this.path = config.GetConfig().getInt("Path");
		}
		if(config.GetConfig().isSet("Location")){
			this.location = getLocation((config.GetConfig().getString("Location")));
		}
		if(config.GetConfig().isSet("Finished")){
			this.finished = config.GetConfig().getBoolean("Finished");
		}
		if(config.GetConfig().isString("Date")){
			this.date = config.GetConfig().getString("Date");
		}
	}

	public void save(){
		config.GetConfig().set("InnerBlocks", innerblocks);
		config.GetConfig().set("OuterBlocks", outerblocks);
		config.GetConfig().set("FloorBlocks", floorblocks);
		config.GetConfig().set("RoofBlocks", roofblocks);
		config.GetConfig().set("Width", this.width);
		config.GetConfig().set("Height", this.height);
		config.GetConfig().set("Path", this.path);
		config.GetConfig().set("Finished", this.finished);
		config.GetConfig().set("Date", this.date);
		config.GetConfig().set("Location", locationToString(this.location));
		config.SaveConfig();
	}
	
	public void delete(){
		this.config.getFile().delete();
		Generator genny;
		try {
			genny = GeneratorManager.getInstance().getGenerator(this);
		} catch (Exception e) {
			genny = GeneratorManager.getInstance().newGenerator(this);
		}
		genny.stop();
		genny.removeMaze();
	}
	
	public void delete(Player player){
		this.config.getFile().delete();
		Generator genny;
		try {
			genny = GeneratorManager.getInstance().getGenerator(this);
		} catch (Exception e) {
			genny = GeneratorManager.getInstance().newGenerator(this);
		}
		genny.stop();
		genny.setPlayer(player);
		genny.removeMaze();
	}
	
	public void remove(Player player){
		Generator genny;
		try {
			genny = GeneratorManager.getInstance().getGenerator(this);
		} catch (Exception e) {
			genny = GeneratorManager.getInstance().newGenerator(this);
		}
		genny.stop();
		genny.setPlayer(player);
		genny.removeMaze();
	}

	public void check(){
		if(width<=6){
			setWidth(6);
		}
		if(height<=1){
			setHeight(2);
		}
		if(!((width/4)+1>=path)){
			setWidth((path-1)*4);
		}
		if(this.innerblocks.size()>0&&this.outerblocks.size()>0&&this.floorblocks.size()>0){
			setFinished(true);
		}
	}

	public String getName(){
		return name;
	}

	public List<String> getInnerBlocks(){
		return innerblocks;
	}

	public void setInnerBlocks(List<String> innerblocks){
		this.innerblocks = innerblocks;
	}

	public void addInnerBlock(Material block){
		String name = block.name();
		this.innerblocks.add(name);
	}

	public void addInnerBlock(Material block, int data){
		String name = block.name();
		this.innerblocks.add(name + ":" + data);
	}

	public List<String> getOuterBlocks(){
		return outerblocks;
	}

	public void setOuterBlocks(List<String> Outerblocks){
		this.outerblocks = Outerblocks;
	}

	public void addOuterBlock(Material block){
		String name = block.name();
		this.outerblocks.add(name);
	}

	public void addOuterBlock(Material block, int data){
		String name = block.name();
		this.outerblocks.add(name + ":" + data);
	}

	public List<String> getFloorBlocks(){
		return floorblocks;
	}

	public void setFloorBlocks(List<String> blocks){
		this.floorblocks = blocks;
	}

	public void addFloorBlock(Material block){
		String name = block.name();
		this.floorblocks.add(name);
	}

	public void addFloorBlock(Material block, int data){
		String name = block.name();
		this.floorblocks.add(name + ":" + data);
	}

	public List<String> getRoofBlocks(){
		return roofblocks;
	}

	public void setRoofBlocks(List<String> blocks){
		this.roofblocks = blocks;
	}

	public void addRoofBlock(Material block){
		String name = block.name();
		this.roofblocks.add(name);
	}

	public void addRoofBlock(Material block, int data){
		String name = block.name();
		this.roofblocks.add(name + ":" + data);
	}

	public int getWidth(){
		return this.width;
	}

	public void setWidth(int i){
		this.width = i;
	}
	
	public boolean checkWidthPath(){
		int realwidth = width-2;
        int lines = (int)Math.floor((double)realwidth/(double)(path+1));
        int i = (path+1) * lines + path;
        return i == realwidth;
	}
	
	public int getNextWidthPath(){
		int realwidth = width-2;
        int lines = (int)Math.floor((double)realwidth/(double)(path+1));
        int i = (path+1) * lines + path;
        return i + 2;
	}
	
	public int getLastWidthPath(){
		int realwidth = width-2-path;
        int lines = (int)Math.floor((double)realwidth/(double)(path+1));
        int i = (path+1) * lines + path;
        return i + 2;
	}

	public int getHeight(){
		return this.height;
	}

	public void setHeight(int i){
		this.height = i;
	}

	public int getPath(){
		return this.path;
	}

	public void setPath(int i){
		this.path = i;
	}

	public Location getLocation(){
		return location;
	}
	
	public String getRawLocation(){
		return locationToString(this.location);
	}

	public boolean isFinished(){
		return this.finished;
	}

	public void setFinished(boolean value){
		if(this.finished!=value){
			this.finished = value;
		}
	}

	public void setDate(){
		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
		this.date = format.format(now);
		config.GetConfig().set("Date", this.date);
		config.SaveConfig();
	}

	public String getDate(){
		return this.date;
	}

	public boolean isLocationIn(Location location){
		Location temp = getLocation();
		return (temp.getWorld()==location.getWorld()&&temp.getBlockX()-((int)Math.floor((double)width / 2))<=location.getBlockX()&&temp.getBlockX()+((int)Math.ceil((double)width / 2))>=location.getBlockX()&&
				temp.getBlockY()-1<=location.getBlockY()&&temp.getBlockY()+height>=location.getBlockY()&&
				temp.getBlockZ()-((int)Math.floor((double)width / 2))<=location.getBlockZ()&&temp.getBlockZ()+((int)Math.ceil((double)width / 2))>=location.getBlockZ());
	}

	public boolean doesMazeOverlap(Maze maze){
		if(!intersectsDimension(maze.getXMin(), maze.getXMax(), getXMin(), getXMax()))
			return false;

		if(!intersectsDimension(maze.getZMin(), maze.getZMax(), getZMin(), getZMax()))
			return false;
		
		if(!intersectsDimension(maze.getYMin(), maze.getYMax(), getYMin(), getYMax()))
			return false;

		return true;
	}

	public boolean intersectsDimension(int aMin, int aMax, int bMin, int bMax){
		return aMin <= bMax && aMax >= bMin;
	}
	
	public int getXMin(){
		return getLocation().getBlockX()-width/2;
	}
	
	public int getXMax(){
		return getLocation().getBlockX()+width/2;
	}
	
	public int getYMin(){
		return getLocation().getBlockY()-1;
	}
	
	public int getYMax(){
		return getLocation().getBlockY() + height;
	}
	
	public int getZMin(){
		return getLocation().getBlockZ()-width/2;
	}
	
	public int getZMax(){
		return getLocation().getBlockZ()+width/2;
	}

	public Location getNorthEasternLowestLocation(){
		return getLocation().add((int)width/2, -1, (int)width/2);
	}

	public Location getNorthEasternHighestLocation(){
		return getLocation().add((int)width/2, this.height, (int)width/2);
	}

	public Location getNorthWesternLowestLocation(){
		return getLocation().add(-(int)width/2, -1, (int)width/2);
	}

	public Location getNorthWesternHighestLocation(){
		return getLocation().add(-(int)width/2, this.height, (int)width/2);
	}

	public Location getSouthEasternLowestLocation(){
		return getLocation().add((int)width/2, -1, -(int)width/2);
	}

	public Location getSouthEasternHighestLocation(){
		return getLocation().add((int)width/2, this.height, -(int)width/2);
	}

	public Location getSouthWesternLowestLocation(){
		return getLocation().add(-(int)width/2, -1, -(int)width/2);
	}

	public Location getSouthWesternHighestLocation(){
		return getLocation().add(-(int)width/2, this.height, -(int)width/2);
	}

	private Location getLocation(String location){
		if(location!=null){
			String[] args = location.split(", ");
			if(args.length>3){
				if(args.length>5){
					return new Location(Bukkit.getWorld(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]),
							Float.parseFloat(args[4]), Float.parseFloat(args[5]));
				}else{
					return new Location(Bukkit.getWorld(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]));
				}
			}
		}
		return null;
	}

	private String locationToString(Location location){
		if(location!=null){
			return location.getWorld().getName() + ", " + location.getX() + ", " + location.getY() + ", " + location.getZ() + ", " + location.getYaw() + ", " + location.getPitch();
		}
		return null;
	}
}
