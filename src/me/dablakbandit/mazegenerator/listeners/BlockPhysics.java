package me.dablakbandit.mazegenerator.listeners;

import me.dablakbandit.mazegenerator.maze.MazeManager;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;

public class BlockPhysics implements Listener{

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockPhysics(BlockPhysicsEvent event){
		if(event.isCancelled()){
			return;
		}
		if(MazeManager.getInstance().isLocationInAMaze(event.getBlock().getLocation())){
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onCreatureSpawn(EntityChangeBlockEvent event){
		if(event.getEntityType().equals(EntityType.FALLING_BLOCK)&&(MazeManager.getInstance().isLocationInAMaze(event.getBlock().getLocation()))){
			event.setCancelled(true);
		}
	}

}
