package me.dablakbandit.mazegenerator.listeners;

import me.dablakbandit.mazegenerator.maze.MazeManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockSpreadEvent;

public class BlockSpread implements Listener{

	@EventHandler
	public void onBlockSpread(BlockSpreadEvent event){
		if(event.isCancelled()){
			return;
		}
		if(MazeManager.getInstance().isLocationInAMaze(event.getBlock().getLocation())){
			event.setCancelled(true);
		}
	}
}
