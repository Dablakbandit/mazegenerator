package me.dablakbandit.mazegenerator.listeners;

import me.dablakbandit.mazegenerator.maze.MazeManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.LeavesDecayEvent;

public class LeavesDecay implements Listener{

	@EventHandler
	public void onLeavesDecay(LeavesDecayEvent event){
		if(event.isCancelled()){
			return;
		}
		if(MazeManager.getInstance().isLocationInAMaze(event.getBlock().getLocation())){
			event.setCancelled(true);
		}
	}
}
