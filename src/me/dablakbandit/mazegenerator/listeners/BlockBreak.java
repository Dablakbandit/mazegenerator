package me.dablakbandit.mazegenerator.listeners;

import me.dablakbandit.mazegenerator.maze.MazeManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreak implements Listener{
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event){
		if(event.isCancelled()){
			return;
		}
		if(!event.getPlayer().hasPermission("mazegenerator.edit")&&MazeManager.getInstance().isLocationInAMaze(event.getBlock().getLocation())){
			event.setCancelled(true);
		}
	}

}
