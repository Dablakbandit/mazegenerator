package me.dablakbandit.mazegenerator.listeners;

import me.dablakbandit.mazegenerator.maze.MazeManager;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteract implements Listener{

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event){
		if(event.isCancelled()){
			return;
		}
		if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK)&&event.getClickedBlock().getType().equals(Material.TNT)
				&&event.getPlayer().getItemInHand().getType().equals(Material.FLINT_AND_STEEL)&&MazeManager.getInstance().isLocationInAMaze(event.getClickedBlock().getLocation())){
			event.setCancelled(true);
		}
	}
}
