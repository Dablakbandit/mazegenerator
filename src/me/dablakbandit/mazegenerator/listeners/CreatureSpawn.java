package me.dablakbandit.mazegenerator.listeners;

import me.dablakbandit.mazegenerator.maze.MazeManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

public class CreatureSpawn implements Listener{
	
	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent event){
		if(event.isCancelled()){
			return;
		}
		if(!event.getSpawnReason().equals(SpawnReason.CUSTOM)&&MazeManager.getInstance().isLocationInAMaze(event.getEntity().getLocation())){
			event.setCancelled(true);
		}
	}

}
