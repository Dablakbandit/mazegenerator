package me.dablakbandit.mazegenerator.listeners;

import java.util.Iterator;

import me.dablakbandit.mazegenerator.maze.MazeManager;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

public class EntityExplode implements Listener{

	@EventHandler
	public void onEntityExplode(EntityExplodeEvent event){
		if(event.isCancelled()){
			return;
		}
		Iterator<Block> iterator = event.blockList().iterator();
		while(iterator.hasNext()) {
			if(MazeManager.getInstance().isLocationInAMaze(iterator.next().getLocation())){
				iterator.remove();
			}
		}
	}

}
