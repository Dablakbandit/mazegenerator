package me.dablakbandit.mazegenerator.listeners;

import me.dablakbandit.mazegenerator.maze.MazeManager;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;

public class BlockFromTo implements Listener{

	@EventHandler
	public void onBlockFromTo(BlockFromToEvent event) {
		if(event.isCancelled()){
			return;
		}
		Block block = event.getBlock();
		if (block.getType() == Material.WATER||block.getType() == Material.STATIONARY_WATER) {
			if(MazeManager.getInstance().isLocationInAMaze(event.getToBlock().getLocation())){
				event.setCancelled(true);
			}
		}
	}
}
