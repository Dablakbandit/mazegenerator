package me.dablakbandit.mazegenerator.listeners;

import me.dablakbandit.mazegenerator.maze.MazeManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ExplosionPrimeEvent;

public class ExplosionPrime implements Listener{
	
	@EventHandler
	public void onExplosionPrime(ExplosionPrimeEvent event){
		if(event.isCancelled()){
			return;
		}
		if(MazeManager.getInstance().isLocationInAMaze(event.getEntity().getLocation())){
			event.setCancelled(true);
		}
	}

}
