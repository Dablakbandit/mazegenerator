package me.dablakbandit.mazegenerator;

import java.io.File;

import me.dablakbandit.mazegenerator.commands.MazeCommand;
import me.dablakbandit.mazegenerator.listeners.BlockBreak;
import me.dablakbandit.mazegenerator.listeners.BlockBurn;
import me.dablakbandit.mazegenerator.listeners.BlockFromTo;
import me.dablakbandit.mazegenerator.listeners.BlockPhysics;
import me.dablakbandit.mazegenerator.listeners.BlockPlace;
import me.dablakbandit.mazegenerator.listeners.BlockSpread;
import me.dablakbandit.mazegenerator.listeners.CreatureSpawn;
import me.dablakbandit.mazegenerator.listeners.EntityExplode;
import me.dablakbandit.mazegenerator.listeners.ExplosionPrime;
import me.dablakbandit.mazegenerator.listeners.LeavesDecay;
import me.dablakbandit.mazegenerator.listeners.PlayerInteract;
import me.dablakbandit.mazegenerator.maze.MazeManager;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class MazeGenerator extends JavaPlugin{

	private static MazeGenerator main;
	private String version = "1.2";
	private String command;
	
	public void onEnable(){
		main = this;
		String version = getConfig().getString("Version");
		if (!version.equals(this.version)){
			File file = new File(getDataFolder(), "config.yml");
			file.delete();
			saveDefaultConfig();
			reloadConfig();
		}
		if(getConfig().isSet("Command")){
			command = getConfig().getString("Command");
		}else{
			getConfig().set("Command", "maze");
			saveConfig();
			command = "maze";
		}
		saveDefaultConfig();
		registerCommands();
		registerListeners();
	}
	
	public void onDisable(){
		MazeManager.getInstance().save();
	}
	
	public static MazeGenerator getInstance(){
		return main;
	}
	 
	private void registerCommands(){
		new MazeCommand(command, "/" + command, "MazeGenerator options");
	}
	
	public String getCommand(){
		return command;
	}
	
	private void registerListeners(){
		Bukkit.getPluginManager().registerEvents(new BlockBreak(), this);
		Bukkit.getPluginManager().registerEvents(new BlockBurn(), this);
		Bukkit.getPluginManager().registerEvents(new BlockFromTo(), this);
		Bukkit.getPluginManager().registerEvents(new BlockPhysics(), this);
		Bukkit.getPluginManager().registerEvents(new BlockPlace(), this);
		Bukkit.getPluginManager().registerEvents(new BlockSpread(), this);
		Bukkit.getPluginManager().registerEvents(new CreatureSpawn(), this);
		Bukkit.getPluginManager().registerEvents(new EntityExplode(), this);
		Bukkit.getPluginManager().registerEvents(new ExplosionPrime(), this);
		Bukkit.getPluginManager().registerEvents(new LeavesDecay(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerInteract(), this);
	}
}
