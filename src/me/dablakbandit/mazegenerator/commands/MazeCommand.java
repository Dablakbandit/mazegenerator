package me.dablakbandit.mazegenerator.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import me.dablakbandit.mazegenerator.AbstractCommand;
import me.dablakbandit.mazegenerator.MazeGenerator;
import me.dablakbandit.mazegenerator.generator.Generator;
import me.dablakbandit.mazegenerator.generator.GeneratorManager;
import me.dablakbandit.mazegenerator.maze.Maze;
import me.dablakbandit.mazegenerator.maze.MazeManager;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MazeCommand extends AbstractCommand{

	public MazeCommand(String command, String usage, String description) {
		super(command, usage, description);
		register();
	}

	public boolean onCommand(CommandSender s, Command cmd, String Label, String[] args)
	{
		if(!s.hasPermission("mazegenerator.admin"))return false;
		if(args.length==0){
			s.sendMessage("===========[ Maze Generator Commands ]===========");
			s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " create");
			s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " info");
			s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " list");
			s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit <args>");
			s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " info <maze>");
			s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " select <maze>");
			s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " new <name>");
			s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " delete <name>");
			s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " load <maze>");
			s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " unload <maze>");
			s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " generator <args>");
			return true;
		}else{
			switch(args[0].toLowerCase()){
			case "list":{
				Map<String, List<String>> mazes = MazeManager.getInstance().listMazes();
				for(String a : mazes.keySet()){
					String list = mazes.get(a).toString();
					if(list.substring(1, list.length()-1).length()!=0){
						s.sendMessage("[" + a + "] " + list.substring(1, list.length()-1));	
					}else{
						s.sendMessage("[" + a + "] " + ChatColor.ITALIC + "none");
					}
				}
				return true;
			}
			case "info":{
				if(args.length==1){
					if(s instanceof Player){
						Maze maze = MazeManager.getInstance().getPlayerSelectedMaze((Player)s);
						if(maze==null){
							s.sendMessage("===========[ Maze Generator Info ]===========");
							s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " info <maze>");
							return false;
						}
						s.sendMessage("===========[ Maze Generator Info ]===========");
						s.sendMessage("Maze " + maze.getName() + " info");
						s.sendMessage("InnerBlocks : " + maze.getInnerBlocks().toString());
						s.sendMessage("OuterBlocks : " + maze.getOuterBlocks().toString());
						s.sendMessage("FloorBlocks : " + maze.getFloorBlocks().toString());
						s.sendMessage("RoofBlocks : " + maze.getRoofBlocks().toString());
						s.sendMessage("Height : " + maze.getHeight());
						s.sendMessage("Width : " + maze.getWidth());
						s.sendMessage("Path : " + maze.getPath());
						s.sendMessage("Date : " + maze.getDate());
						s.sendMessage("Location : " + maze.getRawLocation());
						return true;
					}else{
						s.sendMessage("===========[ Maze Generator Info ]===========");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " info <maze>");
						return false;
					}
				}else{
					Maze maze = MazeManager.getInstance().getMaze(args[1]);
					if(maze==null){
						s.sendMessage("[MazeGen] Unknown maze " + args[1]);
						return false;
					}
					s.sendMessage("===========[ Maze Generator Info ]===========");
					s.sendMessage("Maze " + maze.getName() + " info");
					s.sendMessage("InnerBlocks : " + maze.getInnerBlocks().toString());
					s.sendMessage("OuterBlocks : " + maze.getOuterBlocks().toString());
					s.sendMessage("FloorBlocks : " + maze.getFloorBlocks().toString());
					s.sendMessage("RoofBlocks : " + maze.getRoofBlocks().toString());
					s.sendMessage("Height : " + maze.getHeight());
					s.sendMessage("Width : " + maze.getWidth());
					s.sendMessage("Path : " + maze.getPath());
					s.sendMessage("Date : " + maze.getDate());
					s.sendMessage("Location : " + maze.getRawLocation());
					return true;
				}
			}
			case "generator":{
				if(!s.hasPermission("mazegenerator.generator")){
					s.sendMessage("You dont have the required permissions to run this command");
					return false;
				}
				if(args.length==1){
					s.sendMessage("===========[ Maze Generator Generator ]===========");
					s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " generator list");
					s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " generator set <args>");
					return true;
				}
				switch(args[1].toLowerCase()){
				case "list":{
					List<String> started = new ArrayList<String>();
					List<String> unstarted = new ArrayList<String>();
					for(Generator genny : GeneratorManager.getInstance().getGenerators()){
						if(genny.hasStarted()){
						    	started.add(genny.getMaze().getName());
						}else{
							unstarted.add(genny.getMaze().getName());
						}
					}
					String started1 = started.toString();
					String unstarted1 = unstarted.toString();
					if(started1.substring(1, started1.length()-1).length()!=0){
						s.sendMessage("[Started] " + started1.substring(1, started1.length()-1));
					}else{
						s.sendMessage("[Started] " + ChatColor.ITALIC + "none");
					}
					if(unstarted1.substring(1, unstarted1.length()-1).length()!=0){
						s.sendMessage("[Unstarted] " + unstarted1.substring(1, unstarted1.length()-1));
					}else{
						s.sendMessage("[Unstarted] " + ChatColor.ITALIC + "none");
					}
					return true;
				}
				case "set":{
					if(args.length==2){
						s.sendMessage("===========[ Maze Generator Generator:Set ]===========");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " generator set bps <int>");
						return true;
					}
					switch(args[2].toLowerCase()){
					case "bps":{
						if(args.length==3){
							s.sendMessage("===========[ Maze Generator Generator:Set:Bps ]===========");
							s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " generator set bps <int>");
							return true;
						}
						int i;
						try{
							i = Integer.parseInt(args[3]);
						}catch(Exception e){
							s.sendMessage("[MazeGen] " + args[3] + " isn't a number");
							return false;
						}
						GeneratorManager.getInstance().setBPS(i);
						s.sendMessage("[MazeGen] Max BPS set to " + i);
						return true;
					}
					default:{
						s.sendMessage("===========[ Maze Generator Generator:Set ]===========");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " generator set bps <int>");
						return false;
					}
					}
				}
				default:{
					s.sendMessage("===========[ Maze Generator Generator ]===========");
					s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " generator list");
					s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " generator set <args>");
					return false;
				}
				}
			}
			case "load":{
				if(!s.hasPermission("mazegenerator.load")){
					s.sendMessage("You dont have the required permissions to run this command");
					return false;
				}
				if(args.length==1){
					s.sendMessage("===========[ Maze Generator Load ]===========");
					s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " load <name>");
					return true;
				}
				if(!MazeManager.getInstance().loadMaze(args[1])){
					s.sendMessage("[MazeGen] Maze " + args[1] + " already loaded or doesn't exist");
					return false;
				}
				s.sendMessage("[MazeGen] Maze loaded");
				return true;
			}
			case "unload":{
				if(!s.hasPermission("mazegenerator.unload")){
					s.sendMessage("You dont have the required permissions to run this command");
					return false;
				}
				if(args.length==1){
					s.sendMessage("===========[ Maze Generator Unload ]===========");
					s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " unload <name>");
					return true;
				}
				if(!MazeManager.getInstance().unloadMaze(args[1])){
					s.sendMessage("[MazeGen] Unknown maze " + args[1]);
					return false;
				}
				s.sendMessage("[MazeGen] Maze unloaded");
				return true;
			}
			case "delete":{
				if(!s.hasPermission("mazegenerator.delete")){
					s.sendMessage("You dont have the required permissions to run this command");
					return false;
				}
				if(!(s instanceof Player)){
					s.sendMessage("Command can only be run by players");
					return false;
				}
				Maze maze = MazeManager.getInstance().getPlayerSelectedMaze((Player)s);
				if(maze==null){
					s.sendMessage("[MazeGen] You have not selected a maze");
					return false;
				}
				MazeManager.getInstance().deleteMaze(maze.getName(), (Player)s);
				return true;
			}
			case "remove":{
				if(!s.hasPermission("mazegenerator.delete")){
					s.sendMessage("You dont have the required permissions to run this command");
					return false;
				}
				if(!(s instanceof Player)){
					s.sendMessage("Command can only be run by players");
					return false;
				}
				Maze maze = MazeManager.getInstance().getPlayerSelectedMaze((Player)s);
				if(maze==null){
					s.sendMessage("[MazeGen] You have not selected a maze");
					return false;
				}
				MazeManager.getInstance().removeMaze(maze.getName(), (Player)s);
				return true;
			}
			case "new":{
				if(!s.hasPermission("mazegenerator.new")){
					s.sendMessage("You dont have the required permissions to run this command");
					return false;
				}
				if(args.length==1){
					s.sendMessage("===========[ Maze Generator New ]===========");
					s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " new <name>");
					return true;
				}
				if(!(s instanceof Player)){
					s.sendMessage("Command can only be run by players");
					return false;
				}
				Maze maze = MazeManager.getInstance().getMaze(args[1]);
				if(maze!=null){
					s.sendMessage("[MazeGen] Maze with that name already exists");
					return false;
				}
				maze = MazeManager.getInstance().createNewMaze(args[1], ((Player)s).getLocation());
				MazeManager.getInstance().setPlayerSelectedMaze((Player)s, maze);
				s.sendMessage("[MazeGen] Maze " + args[1] + " created");
				return true;
			}
			case "create":{
				if(!s.hasPermission("mazegenerator.create")){
					s.sendMessage("You dont have the required permissions to run this command");
					return false;
				}
				if(!(s instanceof Player)){
					s.sendMessage("Command can only be run by players");
					return false;
				}
				Maze maze = MazeManager.getInstance().getPlayerSelectedMaze((Player)s);
				if(maze==null){
					s.sendMessage("[MazeGen] You have not selected a maze");
					return false;
				}
				Generator genny = GeneratorManager.getInstance().newGenerator(maze);
				genny.setPlayer((Player)s);
				try {
					genny.generateMaze();
				} catch (Exception e) {
					s.sendMessage(e.getMessage());
				}
				return true;
			}
			case "select":{
				if(!s.hasPermission("mazegenerator.select")){
					s.sendMessage("You dont have the required permissions to run this command");
					return false;
				}
				if(args.length==1){
					s.sendMessage("===========[ Maze Generator Select ]===========");
					s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " select <maze>");
					return true;
				}
				if(!(s instanceof Player)){
					s.sendMessage("Command can only be run by players");
					return false;
				}
				Maze maze = MazeManager.getInstance().getMaze(args[1]);
				if(maze==null){
					s.sendMessage("[MazeGen] Unknown maze");
					return false;
				}
				MazeManager.getInstance().setPlayerSelectedMaze((Player)s, maze);
				return true;
			}
			case "edit":{
				if(!s.hasPermission("mazegenerator.edit")){
					s.sendMessage("You dont have the required permissions to run this command");
					return false;
				}
				if(!(s instanceof Player)){
					s.sendMessage("Command can only be run by players");
					return false;
				}
				if(args.length==1){
					s.sendMessage("===========[ Maze Generator Edit ]===========");
					s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add <args>");					
					s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit set <args>");					
					return true;
				}
				Maze maze = MazeManager.getInstance().getPlayerSelectedMaze((Player)s);
				if(maze==null){
					s.sendMessage("[MazeGen] You have not selected a maze");
					return false;
				}
				switch(args[1].toLowerCase()){
				case "add":{
					if(args.length==2){
						s.sendMessage("===========[ Maze Generator Edit:Add ]===========");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add inner <Material>");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add outer <Material>");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add floor <Material>");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add roof <Material>");
						return true;
					}
					switch(args[2].toLowerCase()){
					case "inner":{
						if(args.length==3){
							s.sendMessage("===========[ Maze Generator Edit:Add:Inner ]===========");
							s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add inner <Material>");
							return true;
						}
						try{
							if(args[3].contains(":")){
								maze.addInnerBlock(Material.valueOf(args[3].split(":")[0].toUpperCase()), Integer.parseInt(args[3].split(":")[1]));
							}else{
								maze.addInnerBlock(Material.valueOf(args[3].toUpperCase()));
							}
						}catch(Exception e){
							s.sendMessage("[MazeGen] Unknown material " + args[3].toUpperCase());
							return false;
						}
						s.sendMessage("[MazeGen] Material " + args[3] + " added to inner blocks");
						return true;
					}
					case "outer":{
						if(args.length==3){
							s.sendMessage("===========[ Maze Generator Edit:Add:Outer ]===========");
							s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add outer <Material>");
							return true;
						}
						try{
							if(args[3].contains(":")){
								maze.addOuterBlock(Material.valueOf(args[3].split(":")[0].toUpperCase()), Integer.parseInt(args[3].split(":")[1]));
							}else{
								maze.addOuterBlock(Material.valueOf(args[3].toUpperCase()));
							}
						}catch(Exception e){
							s.sendMessage("[MazeGen] Unknown material " + args[3].toUpperCase());
							return false;
						}
						s.sendMessage("[MazeGen] Material " + args[3] + " added to outer blocks");
						return true;
					}
					case "floor":{
						if(args.length==3){
							s.sendMessage("===========[ Maze Generator Edit:Add:Floor ]===========");
							s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add floor <Material>");
							return true;
						}
						try{
							if(args[3].contains(":")){
								maze.addFloorBlock(Material.valueOf(args[3].split(":")[0].toUpperCase()), Integer.parseInt(args[3].split(":")[1]));
							}else{
								maze.addFloorBlock(Material.valueOf(args[3].toUpperCase()));
							}
						}catch(Exception e){
							s.sendMessage("[MazeGen] Unknown material " + args[3].toUpperCase());
							return false;
						}
						s.sendMessage("[MazeGen] Material " + args[3].toUpperCase() + " added to floor blocks");
						return true;
					}
					case "roof":{
						if(args.length==3){
							s.sendMessage("===========[ Maze Generator Edit:Add:Roof ]===========");
							s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add roof <Material>");
							return true;
						}
						try{
							if(args[3].contains(":")){
								maze.addRoofBlock(Material.valueOf(args[3].split(":")[0].toUpperCase()), Integer.parseInt(args[3].split(":")[1]));
							}else{
								maze.addRoofBlock(Material.valueOf(args[3].toUpperCase()));
							}
						}catch(Exception e){
							s.sendMessage("[MazeGen] Unknown material " + args[3].toUpperCase());
							return false;
						}
						s.sendMessage("[MazeGen] Material " + args[3].toUpperCase() + " added to roof blocks");
						return true;
					}
					default:{
						s.sendMessage("===========[ Maze Generator Edit:Add ]===========");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add inner <Material>");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add outer <Material>");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add floor <Material>");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add roof <Material>");
						return false;
					}
					}
				}
				case "set":{
					if(args.length==2){
						s.sendMessage("===========[ Maze Generator Edit:Set ]===========");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit set height <int>");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit set path <int>");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit set width <int>");
						return true;
					}
					switch(args[2].toLowerCase()){
					case "height":{
						int temp;
						try{
							temp = Integer.parseInt(args[3]);
						}catch(NumberFormatException e){
							s.sendMessage("[MazeGen] " + args[3] + " isn't a number");
							return false;
						}
						maze.setHeight(temp);
						s.sendMessage("[MazeGen] Set height to " + temp);
						return true;
					}
					case "path":{
						int temp;
						try{
							temp = Integer.parseInt(args[3]);
						}catch(NumberFormatException e){
							s.sendMessage("[MazeGen] " + args[3] + " isn't a number");
							return false;
						}
						maze.setPath(temp);
						s.sendMessage("[MazeGen] Set path to " + temp);
						if(!maze.checkWidthPath()){
							s.sendMessage("[MazeGen] Maze width is not compatable with the path, use " + maze.getNextWidthPath() + " or " + maze.getLastWidthPath());
						}
						return true;
					}
					case "width":{
						int temp;
						try{
							temp = Integer.parseInt(args[3]);
						}catch(NumberFormatException e){
							s.sendMessage("[MazeGen] " + args[3] + " isn't a number");
							return false;
						}
						maze.setWidth(temp);
						s.sendMessage("[MazeGen] Set width to " + temp);
						if(!maze.checkWidthPath()){
							s.sendMessage("[MazeGen] Maze width is not compatable with the path, use " + maze.getNextWidthPath() + " or " + maze.getLastWidthPath());
						}
						return true;
					}
					default:{
						s.sendMessage("===========[ Maze Generator Edit:Set ]===========");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit set height <int>");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit set path <int>");
						s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit set width <int>");
						return false;
					}
					}
				}
				default:
					s.sendMessage("===========[ Maze Generator Edit ]===========");
					s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit add <args>");					
					s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit set <args>");					
					return false;
				}
			}
			default:{
				s.sendMessage("===========[ Maze Generator Commands ]===========");
				s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " create");
				s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " info");
				s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " list");
				s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " edit <args>");
				s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " info <maze>");
				s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " select <maze>");
				s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " new <name>");
				s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " delete <name>");
				s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " load <maze>");
				s.sendMessage("/" + MazeGenerator.getInstance().getCommand() + " unload <maze>");
				return false;
			}
			}
		}
	}
}
