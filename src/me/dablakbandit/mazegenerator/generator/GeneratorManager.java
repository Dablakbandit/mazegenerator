package me.dablakbandit.mazegenerator.generator;

import java.util.ArrayList;
import java.util.List;

import me.dablakbandit.mazegenerator.MazeGenerator;
import me.dablakbandit.mazegenerator.maze.Maze;

public class GeneratorManager {

	private List<Generator> generators = new ArrayList<Generator>();
	public static GeneratorManager main = new GeneratorManager();
	private int maxbps;
	
	public GeneratorManager(){
		this.maxbps = getMaxBPS();
	}
	
	public static GeneratorManager getInstance(){
		return main;
	}
	
	public Generator newGenerator(Maze maze){
		try{
			return getGenerator(maze);
		}catch(Exception e){
			Generator generator = new Generator(maze);
			this.generators.add(generator);
			return generator;
		}
	}
	
	public Generator getGenerator(Maze maze) throws Exception{
		for(Generator generator : generators){
			if(generator.getMaze()!=null&&generator.getMaze().equals(maze)){
				return generator;
			}
		}
		throw new Exception("[MazeGen] No generator found for that maze");
	}
	
	public void removeGenerator(Generator generator){
		if(this.generators.contains(generator)){
			this.generators.remove(generator);
			updateInt();
		}
	}
	
	public void setBPS(int i){
		MazeGenerator.getInstance().getConfig().set("Advanced.Generate_Block_Rate", i);
		MazeGenerator.getInstance().saveConfig();
		updateInt();
	}
	
	public List<Generator> getGenerators(){
		return this.generators;
	}
	
	public void updateInt(){
		int started = 0;
		for(Generator generator : generators){
			if(generator.hasStarted()){
				started++;
			}
		}
		if(started==0){
			return;
		}
		this.maxbps = getMaxBPS();
		int bps = maxbps/started;
		for(Generator generator : generators){
			generator.setbps(bps);
		}
	}
	
	private int getMaxBPS(){
		return MazeGenerator.getInstance().getConfig().getInt("Advanced.Generate_Block_Rate");
	}
}
