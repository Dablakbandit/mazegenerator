package me.dablakbandit.mazegenerator.generator.runnables;

import me.dablakbandit.mazegenerator.MazeGenerator;
import me.dablakbandit.mazegenerator.generator.Generator;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class SetToAirRunnable implements Runnable{

	private long mili;
	private Generator gen;
	private Runnable finish;
	private int curX, curZ, curY;
	private Location clone;

	public SetToAirRunnable(Generator gen, Runnable finish){
		this.gen = gen;
		this.finish = finish;
		this.curX = gen.getMinX();
		this.curZ = gen.getMinZ();
		this.curY = gen.getMinY();
		clone = gen.getCenter().clone();
	}

	@Override
	public void run() {
		this.mili = System.currentTimeMillis();
		for(int i = curX; i <= gen.getMaxX(); i++){
			curX = i;
			clone.setX(i);
			for(int ii = curZ; ii <= gen.getMaxZ(); ii++){
				curZ = ii;
				clone.setZ(ii);
				for(int iii = curY; iii <= gen.getMaxY(); iii++){
					if(!gen.started()){
						return;
					}
					curY = iii;
					if(gen.getBPSCurrent()>=gen.getBPS()){
						gen.resetBPS();
						double seconds = Math.ceil((double)(System.currentTimeMillis() - mili)/100)/10;
						int delay = 0;
						if(seconds<(double)1){
							delay = 20 - (int)((double)20 * seconds);
						}
						if(delay<=0){
							delay = 1;
						}
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(MazeGenerator.getInstance(), this, delay);
						return;
					}
					clone.setY(iii);
					Block b = clone.getBlock();
					if(b.getType()!=Material.AIR){
						gen.incrementBPS();
						b.setType(Material.AIR);
					}
				}
				curY = gen.getMinY();
			}
			curZ = gen.getMinZ();
		}
		finish.run();
		return;
	}

}
