package me.dablakbandit.mazegenerator.generator.runnables;

import java.util.Collections;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import me.dablakbandit.mazegenerator.MazeGenerator;
import me.dablakbandit.mazegenerator.generator.Generator;

public class RoofGenerator implements Runnable{

	private long mili;
	private Generator gen;
	private Runnable finish;
	private int curX, curZ;
	private Location clone;

	public RoofGenerator(Generator gen, Runnable finish){
		this.gen = gen;
		this.finish = finish;
		this.curX = gen.getMinX();
		this.curZ = gen.getMinZ();
		this.clone = gen.getCenter().clone();
		this.clone.setY(gen.getMaxY());
	}

	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		this.mili = System.currentTimeMillis();
		for (int i = curX; i <= gen.getMaxX(); i++) {
			clone.setX(i);
			curX = i;
			for(int ii = curZ; ii <= gen.getMaxZ(); ii++){
				if(!gen.started()){
					return;
				}
				curZ = ii;
				if(gen.getBPSCurrent()>=gen.getBPS()){
					gen.resetBPS();
					double seconds = Math.ceil((double)(System.currentTimeMillis() - mili)/100)/10;
					int delay = 0;
					if(seconds<(double)1){
						delay = 20 - (int)((double)20 * seconds);
					}
					if(delay<=0){
						delay = 1;
					}
					Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(MazeGenerator.getInstance(), this, delay);
					return;
				}
				clone.setZ(ii);
				Block b = clone.getBlock();
				if(b.getType()==Material.AIR){
					Collections.shuffle(gen.getMaze().getRoofBlocks());
					String mat = gen.getMaze().getRoofBlocks().get(0);
					if(mat.contains(":")){
						b.setType(Material.valueOf(mat.split(":")[0]));
						b.setData((byte)Integer.parseInt(mat.split(":")[1]));
					}else{
						b.setType(Material.valueOf(mat));
					}
					gen.incrementBPS();
				}
			}
			curZ = gen.getMinZ();
		}
		finish.run();
	}

}
