package me.dablakbandit.mazegenerator.generator.runnables;

import java.util.Collections;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import me.dablakbandit.mazegenerator.MazeGenerator;
import me.dablakbandit.mazegenerator.generator.Generator;

public class OuterWallXGenerator implements Runnable{

	private long mili;
	private Generator gen;
	private Runnable finish;
	private int curX, curY, maxY;
	private Location clone;

	public OuterWallXGenerator(Generator gen, Runnable finish, int z){
		this.gen = gen;
		this.finish = finish;
		this.curY = gen.getMinY();
		this.curX = gen.getMinX();
		this.clone = gen.getCenter().clone();
		this.clone.setZ(z);
		this.maxY = gen.getMaxY();
		if(gen.getMaze().getRoofBlocks().size()>0)maxY--;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		this.mili = System.currentTimeMillis();
		for (int i = curX; i <= gen.getMaxX(); i++) {
			clone.setX(i);
			curX = i;
			for (int ii = curY; ii <= maxY; ii++) {
				if(!gen.started()){
					return;
				}
				curY = ii;
				if(gen.getBPSCurrent()>=gen.getBPS()){
					gen.resetBPS();
					double seconds = Math.ceil((double)(System.currentTimeMillis() - mili)/100)/10;
					int delay = 0;
					if(seconds<(double)1){
						delay = 20 - (int)((double)20 * seconds);
					}
					if(delay<=0){
						delay = 1;
					}
					Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(MazeGenerator.getInstance(), this, delay);
					return;
				}
				clone.setY(ii);
				Block b = clone.getBlock();
				if(b.getType()==Material.AIR){
					Collections.shuffle(gen.getMaze().getOuterBlocks());
					String mat = gen.getMaze().getOuterBlocks().get(0);
					if(mat.contains(":")){
						b.setType(Material.valueOf(mat.split(":")[0]));
						b.setData((byte)Integer.parseInt(mat.split(":")[1]));
					}else{
						b.setType(Material.valueOf(mat));
					}
					gen.incrementBPS();
				}
			}
			curY = gen.getMinY();
		}
		finish.run();
	}

}
