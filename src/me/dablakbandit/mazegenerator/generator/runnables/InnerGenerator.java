package me.dablakbandit.mazegenerator.generator.runnables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.dablakbandit.mazegenerator.MazeGenerator;
import me.dablakbandit.mazegenerator.generator.Chamber;
import me.dablakbandit.mazegenerator.generator.Generator;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class InnerGenerator implements Runnable{

	private long mili;
	private Generator gen;
	private Runnable finish;
	private int curX, curZ, curY, maxY;
	private Location clone;

	public InnerGenerator(Generator gen, Runnable finish){
		this.gen = gen;
		this.finish = finish;
		this.maxY = gen.getMaxY();
		this.clone = gen.getCenter().clone();
		if(gen.getMaze().getRoofBlocks().size()>0)maxY--;
		Chamber c = new Chamber(gen.getMaze().getWidth(), gen.getMaze().getWidth(), 0, 0);
		queue.add(c);
		third = gen.getMaze().getWidth()/3;
	}

	private List<Chamber> queue = new ArrayList<Chamber>();

	int third = 100;

	private Chamber chamber;
	private List<Integer> holes;
	private int poss;

	@Override
	public void run() {
		if(!gen.started()){
			return;
		}
		if(queue.isEmpty()){
			finish.run();
			return;
		}
		chamber = queue.get(0);
		if(chamber.getMaxX()-chamber.getMinX() <= gen.getMaze().getPath()*2	|| chamber.getMaxZ() - chamber.getMinZ() <= gen.getMaze().getPath()*2) {
			queue.remove(0);
			run();
			return;
		}
		int hole;
		int hole1;
		String orientation = chamber.getOrientation();
		holes = new ArrayList<Integer>();
		switch (orientation) {
		case "x":
			poss = getRandom(chamber.getMinZ()+ gen.getMaze().getPath()+1, chamber.getMaxZ(), gen.getMaze().getPath()+1);
			hole = getRandom(chamber.getMinX() + gen.getMaze().getPath(), chamber.getMaxX(), gen.getMaze().getPath()+1);
			holes.add(gen.getMinX()+hole);
			if(chamber.getMaxX()-chamber.getMinX()-gen.getMaze().getPath()>third){
				hole1 = chamber.getMinX();
				do{
					hole1 = getRandom(chamber.getMinX() + gen.getMaze().getPath(), chamber.getMaxX(), gen.getMaze().getPath()+1);
				}while(!near(hole1, hole));
				holes.add(gen.getMinX()+hole1);
			}
			queue.remove(0);
			clone.setZ(gen.getMinZ() + poss);
			curX = gen.getMinX() + chamber.getMinX();
			curY = gen.getMinY();
			case_x();
			return;
		case "z":
			poss = getRandom(chamber.getMinX()+ gen.getMaze().getPath() + 1, chamber.getMaxX(), gen.getMaze().getPath()+1);
			hole = getRandom(chamber.getMinZ()+ gen.getMaze().getPath(), chamber.getMaxZ(), gen.getMaze().getPath()+1);
			holes.add(gen.getMinZ()+hole);
			if(chamber.getMaxZ()-chamber.getMinZ()-gen.getMaze().getPath()>third){
				hole1 = chamber.getMinZ();
				do{
					hole1 = getRandom(chamber.getMinZ()+ gen.getMaze().getPath(), chamber.getMaxZ(), gen.getMaze().getPath()+1);
				}while(!near(hole1, hole));
				holes.add(gen.getMinZ()+hole1);
			}
			queue.remove(0);
			clone.setX(gen.getMinX() + poss);
			curZ = gen.getMinZ() + chamber.getMinZ();
			curY = gen.getMinY();
			case_z();
			return;
		}
	}

	private int getRandom(int min, int max, int i){
		List<Integer> list = new ArrayList<Integer>();
		for(int a = min; a < max; a+=i){
			list.add(a);
		}
		Collections.shuffle(list);
		return list.get(0);
	}

	public boolean near(int isntNear, int here){
		if(isntNear<=here-gen.getMaze().getPath()){
			return true;
		}
		if(isntNear>here){
			return true;
		}
		return false;
	}

	private boolean has(List<Integer> holes, int i){
		for(int ii : holes){
			if(!near(i, ii))return false;
		}
		return true;
	}

	@SuppressWarnings("deprecation")
	public void case_x(){
		this.mili = System.currentTimeMillis();
		for(int x = curX; x < gen.getMinX() + chamber.getMaxX(); x++){
			clone.setX(x);
			curX = x;
			if(has(holes, x)){
				for(int y = curY; y <= maxY; y++){
					curY = y;
					if(!gen.started()){
						return;
					}
					if(gen.getBPSCurrent()>=gen.getBPS()){
						gen.resetBPS();
						double seconds = Math.ceil((double)(System.currentTimeMillis() - mili)/100)/10;
						int delay = 0;
						if(seconds<(double)1){
							delay = 20 - (int)((double)20 * seconds);
						}
						if(delay<=0){
							delay = 1;
						}
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(MazeGenerator.getInstance(), new Runnable(){
							@Override
							public void run() {
								case_x();
							}
						}, delay);
						return;
					}
					clone.setY(y);
					Block b = clone.getBlock();
					if(b.getType()==Material.AIR){
						Collections.shuffle(gen.getMaze().getInnerBlocks());
						String mat = gen.getMaze().getInnerBlocks().get(0);
						if(mat.contains(":")){
							b.setType(Material.valueOf(mat.split(":")[0]));
							b.setData((byte)Integer.parseInt(mat.split(":")[1]));
						}else{
							b.setType(Material.valueOf(mat));
						}
						gen.incrementBPS();
					}
				}
				curY = gen.getMinY();
			}
		}
		Chamber c = new Chamber(chamber.getMaxX(), poss, chamber.getMinX(), chamber.getMinZ());
		Chamber c1 = new Chamber(chamber.getMaxX(), chamber.getMaxZ(), chamber.getMinX(), poss);
		queue.add(c);
		queue.add(c1);
		run();
	}

	@SuppressWarnings("deprecation")
	public void case_z(){
		this.mili = System.currentTimeMillis();
		for(int z = curZ; z < gen.getMinZ() + chamber.getMaxZ(); z++){
			curZ = z;
			clone.setZ(z);
			if(has(holes, z)){
				for(int y = curY; y <= maxY; y++){
					curY = y;
					if(!gen.started()){
						return;
					}
					if(gen.getBPSCurrent()>=gen.getBPS()){
						gen.resetBPS();
						double seconds = Math.ceil((double)(System.currentTimeMillis() - mili)/100)/10;
						int delay = 0;
						if(seconds<(double)1){
							delay = 20 - (int)((double)20 * seconds);
						}
						if(delay<=0){
							delay = 1;
						}
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(MazeGenerator.getInstance(), new Runnable(){
							@Override
							public void run() {
								case_z();
							}
						}, delay);
						return;
					}
					clone.setY(y);
					Block b = clone.getBlock();
					if(b.getType()==Material.AIR){
						Collections.shuffle(gen.getMaze().getInnerBlocks());
						String mat = gen.getMaze().getInnerBlocks().get(0);
						if(mat.contains(":")){
							b.setType(Material.valueOf(mat.split(":")[0]));
							b.setData((byte)Integer.parseInt(mat.split(":")[1]));
						}else{
							b.setType(Material.valueOf(mat));
						}
						gen.incrementBPS();
					}
				}
				curY = gen.getMinY();
			}
		}
		Chamber c = new Chamber(poss, chamber.getMaxZ(), chamber.getMinX(), chamber.getMinZ());
		Chamber c1 = new Chamber(chamber.getMaxX(), chamber.getMaxZ(), poss, chamber.getMinZ());
		queue.add(c);
		queue.add(c1);
		run();
	}

}
