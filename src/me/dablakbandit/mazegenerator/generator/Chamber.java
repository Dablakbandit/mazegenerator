package me.dablakbandit.mazegenerator.generator;

import java.util.Random;

public class Chamber {
	private int maxX, maxZ;
	private int minX, minZ;
	private static Random r = new Random();

	public Chamber(int maxX, int maxZ, int minX, int minZ){
		this.maxX = maxX;
		this.maxZ = maxZ;
		this.minX = minX;
		this.minZ = minZ;
	}
	
	public int getMaxX(){
		return maxX;
	}
	
	public int getMaxZ(){
		return maxZ;
	}
	
	public int getMinX(){
		return minX;
	}
	
	public int getMinZ(){
		return minZ;
	}
	
	public String getOrientation() {
		if(maxX-minX > maxZ-minZ){
			return "z";
		}else if(maxZ-minZ > maxX-minX){
			return "x";
		}else{
			int g = r.nextInt(1);
			if(g == 0){
				return "x";
			}else{
				return "z";
			}
		}
	}
}
