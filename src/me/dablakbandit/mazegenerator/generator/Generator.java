package me.dablakbandit.mazegenerator.generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import me.dablakbandit.mazegenerator.MazeGenerator;
import me.dablakbandit.mazegenerator.generator.runnables.FloorGenerator;
import me.dablakbandit.mazegenerator.generator.runnables.InnerGenerator;
import me.dablakbandit.mazegenerator.generator.runnables.OuterWallXGenerator;
import me.dablakbandit.mazegenerator.generator.runnables.OuterWallZGenerator;
import me.dablakbandit.mazegenerator.generator.runnables.RoofGenerator;
import me.dablakbandit.mazegenerator.generator.runnables.SetToAirRunnable;
import me.dablakbandit.mazegenerator.maze.Maze;
import me.dablakbandit.mazegenerator.maze.MazeManager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

@SuppressWarnings("deprecation")
public class Generator {

	private Maze maze;
	private int bps;
	private int bps_current = 0;
	private boolean started;
	private Location center;
	private int minX, minY, minZ, maxX, maxY, maxZ;
	private String player;
	@SuppressWarnings("unused")
	private long start, mili; 

	public Generator(Maze maze){
		this.maze = maze;
	}

	public int getBPS(){
		return bps;
	}

	public int getBPSCurrent(){
		return bps_current;
	}

	public void incrementBPS(){
		bps_current++;
	}

	public void resetBPS(){
		bps_current = 0;
	}

	public Location getCenter(){
		return center;
	}

	public int getMinX(){
		return minX;
	}

	public int getMinY(){
		return minY;
	}

	public int getMinZ(){
		return minZ;
	}

	public int getMaxX(){
		return maxX;
	}

	public int getMaxY(){
		return maxY;
	}

	public int getMaxZ(){
		return maxZ;
	}

	public void generateMaze() throws Exception{
		if(hasStarted()){
			throw new Exception("[MazeGen] Maze already being generated");
		}
		maze.check();
		if(!maze.isFinished()){
			GeneratorManager.getInstance().removeGenerator(this);
			throw new Exception("[MazeGen] Maze configuration not complete");
		}
		this.center = maze.getLocation();
		int i = (int)Math.floor((double)maze.getWidth() / 2)-1;
		int ii = (int)Math.ceil((double)maze.getWidth() / 2);
		this.minX = center.getBlockX() - i;
		this.minY = center.getBlockY() - 1;
		this.minZ = center.getBlockZ() - i;
		this.maxX = center.getBlockX() + ii;
		this.maxY = center.getBlockY() + maze.getHeight();
		this.maxZ = center.getBlockZ() + ii;
		start();
	}

	public void setPlayer(Player player){
		this.player = player.getUniqueId().toString();
	}

	public void setbps(int i){
		this.bps = i;
		sendMessage("[MazeGen] " + maze.getName() + ": Set BPS to " + this.bps + "BPS");
	}

	public boolean hasStarted(){
		return started;
	}

	public Maze getMaze(){
		return this.maze;
	}

	private void start(){
		this.started = true;
		GeneratorManager.getInstance().updateInt();
		sendMessage("[MazeGen] " + maze.getName() + ": Checking for overlapping ");
		String temp = MazeManager.getInstance().isMazeOverlapping(maze);
		if(temp!=null){
			sendMessage("[MazeGen] " + maze.getName() + ": Cannot create, " + temp);
			GeneratorManager.getInstance().removeGenerator(this);
			return;
		}
		sendMessage("[MazeGen] " + maze.getName() + ": Setting all to air @ " + this.bps + "BPS");
		this.mili = System.currentTimeMillis();
		this.start = mili;
		setToAir();
	}

	public void stop(){
		if(this.started){
			this.started = false;
			sendMessage("[MazeGen] " + maze.getName() + ": Stopped");
		}
	}

	public void sendMessage(String message){
		try{
			Player player = Bukkit.getPlayer(UUID.fromString(this.player));
			if(player!=null){
				player.sendMessage(message);
			}
		}catch(Exception e){

		}
	}

	public void removeMaze(){
		this.center = maze.getLocation();
		int i = (int)Math.floor((double)maze.getWidth() / 2)-1;
		int ii = (int)Math.ceil((double)maze.getWidth() / 2);
		this.minX = center.getBlockX() - i;
		this.minY = center.getBlockY() - 1;
		this.minZ = center.getBlockZ() - i;
		this.maxX = center.getBlockX() + ii;
		this.maxY = center.getBlockY() + maze.getHeight();
		this.maxZ = center.getBlockZ() + ii;
		this.started = true;
		GeneratorManager.getInstance().updateInt();
		this.mili = System.currentTimeMillis();
		this.start = mili;
		sendMessage("[MazeGen] " + maze.getName() + ": Removing @ " + this.bps + "BPS");
		removeMaze(0);
	}

	private void removeMaze(final int count){
		new SetToAirRunnable(this, new Runnable(){
			@Override
			public void run() {
				sendMessage("[MazeGen] " + maze.getName() + ": Removed");
				finish();
			}
		}).run();
	}

	private void setToAir(){
		new SetToAirRunnable(this, new Runnable(){
			@Override
			public void run() {
				sendMessage("[MazeGen] " + maze.getName() + ": Set all to air");
				sendMessage("[MazeGen] " + maze.getName() + ": Creating floor @ " + bps + "BPS");
				createFloor();
			}
		}).run();
	}

	//TODO
	private void createFloor(){
		new FloorGenerator(this, new Runnable(){
			@Override
			public void run() {
				sendMessage("[MazeGen] " + maze.getName() + ": Created floor");
				if(maze.getRoofBlocks().size()==0){
					createWall1();
					sendMessage("[MazeGen] " + maze.getName() + ": Creating wall-1 @ " + bps + "BPS");
				}else{
					sendMessage("[MazeGen] " + maze.getName() + ": Creating roof @ " + bps + "BPS");
					createRoof(0);
				}
			}
		}).run();
	}

	private void createRoof(final int count){
		new RoofGenerator(this, new Runnable(){
			@Override
			public void run() {
				sendMessage("[MazeGen] " + maze.getName() + ": Created Roof");
				sendMessage("[MazeGen] " + maze.getName() + ": Creating wall-1 @ " + bps + "BPS");
				createWall1();
			}
		}).run();
	}

	private void createWall1(){
		new OuterWallXGenerator(this, new Runnable(){
			@Override
			public void run() {
				sendMessage("[MazeGen] " + maze.getName() + ": Created Wall-1");
				sendMessage("[MazeGen] " + maze.getName() + ": Creating wall-2 @ " + bps + "BPS");
				createWall2();
			}
		}, minZ).run();
	}

	private void createWall2(){
		new OuterWallXGenerator(this, new Runnable(){
			@Override
			public void run() {
				sendMessage("[MazeGen] " + maze.getName() + ": Created Wall-2");
				sendMessage("[MazeGen] " + maze.getName() + ": Creating wall-3 @ " + bps + "BPS");
				createWall3();
			}
		}, maxZ).run();
	}

	private void createWall3(){
		new OuterWallZGenerator(this, new Runnable(){
			@Override
			public void run() {
				sendMessage("[MazeGen] " + maze.getName() + ": Created Wall-2");
				sendMessage("[MazeGen] " + maze.getName() + ": Creating wall-3 @ " + bps + "BPS");
				createWall4();
			}
		}, minX).run();
	}

	private void createWall4(){
		new OuterWallZGenerator(this, new Runnable(){
			@Override
			public void run() {
				sendMessage("[MazeGen] " + maze.getName() + ": Created Wall-4");
				sendMessage("[MazeGen] " + maze.getName() + ": Creating Innards @ " + bps + "BPS");
				Chamber c = new Chamber(maze.getWidth(), maze.getWidth(), 0, 0);
				queue.add(c);
				third = maze.getWidth()/3;
				createInnards2();
			}
		}, maxX).run();
	}

	private List<Chamber> queue = new ArrayList<Chamber>();

	int third = 100;

	private void createInnards2(){
		new InnerGenerator(this, new Runnable(){
			@Override
			public void run() {
				sendMessage("[MazeGen] " + maze.getName() + ": Created Innards");
				finish();
			}
		}).run();
	}

	private void finish(){
		GeneratorManager.getInstance().removeGenerator(this);
	}

	public boolean started() {
		return started;
	}
}
